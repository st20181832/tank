using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Rigidbody rb;
    public Transform top;

    public float sidwaysForce = 500f;
    public float forwardForce = 2000f;

    [SerializeField]
    private float currentForward = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        currentForward = forwardForce;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(0, 0, currentForward* Time.deltaTime);
        if (Input.GetKey("w"))
        {
            Start();
        }

        if (Input.GetKey("s"))
        {
            stop();
        }

        if ( Input.GetKey("d"))
        {
            rb.AddForce(sidwaysForce * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey("a"))
        {
            rb.AddForce(-sidwaysForce * Time.deltaTime, 0, 0);
        }
    }

    public void stop()
    {
        currentForward = 0.0f;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{
    public GameObject explosion;

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        this.gameObject.SetActive(false);
        SceneManager.LoadScene("Scene1");
    }
}
